unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.ExtCtrls, Unit2, Unit3;

type
  TForm1 = class(TForm)
    BotaoEditar: TButton;
    BotaoInserir: TButton;
    DBListaPokemon: TDBLookupListBox;
    PainelInformacoes: TPanel;
    Lb_Id: TLabel;
    Lb_Treinador: TLabel;
    Lb_Nome: TLabel;
    Lb_Nivel: TLabel;
    Tx_Id: TDBText;
    Tx_Treinador: TDBText;
    Tx_Nome: TDBText;
    Tx_Nivel: TDBText;
    BotaoDeletar: TButton;
    procedure BotaoInserirClick(Sender: TObject);
    procedure BotaoDeletarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BotaoDeletarClick(Sender: TObject);
begin
  DataModule2.FDQueryPokemon.Delete;
end;

procedure TForm1.BotaoInserirClick(Sender: TObject);
begin
 Form3 := TForm3.Create(Application);
 Form3.Show;
end;

end.
