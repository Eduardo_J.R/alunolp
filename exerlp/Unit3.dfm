object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 262
  ClientWidth = 258
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Lb_Nome: TLabel
    Left = 40
    Top = 56
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Lb_Treinador: TLabel
    Left = 40
    Top = 104
    Width = 46
    Height = 13
    Caption = 'Treinador'
  end
  object Lb_Nivel: TLabel
    Left = 40
    Top = 152
    Width = 23
    Height = 13
    Caption = 'Nivel'
  end
  object DBEdit_Nome: TDBEdit
    Left = 92
    Top = 53
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object DBEdit_Treinador: TDBEdit
    Left = 92
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object DBEdit_Nivel: TDBEdit
    Left = 92
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object BotaoAdicionar: TButton
    Left = 92
    Top = 192
    Width = 85
    Height = 25
    Caption = 'Adicionar'
    TabOrder = 3
  end
end
